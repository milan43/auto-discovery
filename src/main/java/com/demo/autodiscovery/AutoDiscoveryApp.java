package com.demo.autodiscovery;

import com.demo.autodiscovery.dao.CarDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*

 @Author melone
 @Date 6/26/18 
 
 */
public class AutoDiscoveryApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("config/auto-discovery.xml");
        CarDao car = ((CarDao) context.getBean("ferrari"));
        car.printDetail();
    }
}
