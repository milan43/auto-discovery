package com.demo.autodiscovery.domain;

import com.demo.autodiscovery.dao.CarDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*

 @Author melone
 @Date 6/26/18 
 
 */
//@Component
//helps container to scan and create the bean of Class defined by @Component annotation
//using @component bean of Ferrari is automatically created and injected through <context:component-scan/> from xml
public class Ferrari implements CarDao {
    @Value("Ferrari")
    private String name;
    @Value("2000000.00")
    private double price;
    @Value("Red")
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Ferrari{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", color='" + color + '\'' +
                '}';
    }

    public void printDetail() {
        System.out.println(this.name + " " + this.price + " " +this.color);
    }
}
