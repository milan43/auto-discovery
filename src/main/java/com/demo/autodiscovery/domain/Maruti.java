package com.demo.autodiscovery.domain;

import com.demo.autodiscovery.dao.CarDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.swing.*;

/*

 @Author melone
 @Date 6/26/18 
 
 */
public class Maruti implements CarDao {
    @Value("Maruti")
    private String name;
    @Value("1500000")
    private double price;
    @Value("White")
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Ferrari{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", color='" + color + '\'' +
                '}';
    }

    public void printDetail() {
        System.out.println(this.name + " " + this.price + " " +this.color);
    }
}